#ifndef UTILITY_INTERFACE_H
#define UTILITY_INTERFACE_H


#include "basic_types.h"


namespace othello::utility
{

  // Board validation
  /*! Query a a bitboard piece position set representing all players pieces
   * for a given BitBoard */
  BitPieces occupiedPositions(const BitBoard& board);

  /*! Query whether a position on a bitboard piece position set is occupied
   * \param[in] pieces A players pieces on the board
   * \param[in] board_pos The board position
   * \return Returns true if the queried position is occupied */
  bool occupied(const BitPieces& pieces, const BitPos& board_pos);

  /*! Query whether a position on a bitboard is occupied
   * \param[in] board The board
   * \param[in] board_pos The board position
   * \return Returns true if the queried position is occupied */
  bool occupied(const BitBoard& board, const BitPos& board_pos);

  /*! Query the next board position given a direction
   * \param[in] board_pos Board position of search start
   * \param[in] dir Search direction
   * \return Returns the next position */
  BitPos nextPosition(const BitPos& board_pos, const MoveDirection& dir);

  /*! Given a board position and a direction find the bracketing pieces board position
   * \param[in] board The board
   * \param[in] board_pos Board position of search start
   * \param[in] player_id Current player's id
   * \param[in] dir Search direction
   * \return The position of the bracketing piece */
  BitPos findBracketingPiece(const BitBoard& board, const BitPos& board_pos,
                             const PlayerId&      player_id,
                             const MoveDirection& dir);

  //  BracketingPieceSet findBracketingPieceSet(const BitBoard& board, const
  //  BitPos& board_pos);


  /*! Query a players legal moves
   * \param[in] board The board
   * \param[in] player_id Querying player's id
   * \return Set of legal moves */
  BitPosSet legalMoves(const BitBoard& board, const PlayerId& player_id );

  /*! Query wherther a move for a given player is legal for a given bitboard
   * \param[in] board The board
   * \param[in] player_id The player id
   * \param[in] board_pos The board position where the next piece is to be placed
   * \return Returns true if the move is legal */
  bool isLegalMove(const BitBoard& board, const PlayerId&, const BitPos& board_pos);

  /*! Place piece and flip all bracketed pices of oponent player on given board, current player id, and valid board position
   * \param[in,out] board Bit board
   * \param[in] player_id Current player
   * \param[in board_pos Valid piece placement */
  void placeAndFlip(BitBoard& board, const PlayerId& player_id, const BitPos& board_pos );


} // namespace othello::alg





#endif // UTILITY_INTERFACE_H
